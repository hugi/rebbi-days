package is.rebbi.days;

import java.time.LocalDate;
import java.util.function.IntFunction;

public class Day {

	private final String _icelandicName;
	private final String _englishName;
	private final boolean _isFull;
	private final IntFunction<LocalDate> _function;

	public Day( final String icelandicName, final String englishName, final boolean isFull, IntFunction<LocalDate> function ) {
		_icelandicName = icelandicName;
		_englishName = englishName;
		_isFull = isFull;
		_function = function;
	}

	public String icelandicName() {
		return _icelandicName;
	}

	public String englishName() {
		return _englishName;
	}

	public boolean isFull() {
		return _isFull;
	}

	public LocalDate calculateInYear( final int year ) {
		return _function.apply( year );
	}
}
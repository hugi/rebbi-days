package is.rebbi.days;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * FIXME: Increase performance by checking the least expensive dates first.
 * FIXME: Okkur vantar daga sem eru ekki frídagar. Dagar sem gætu passað inn í þetta eru t.d. hvítasunnudagur.
 */

public class Days {

	private final Map<LocalDate, String> _holidayNames;

	/**
	 * The year to get holidays for
	 */
	private final int _year;

	/**
	 * Constructs a new holidays calculator.
	 *
	 * @param newYear The year to calculate holidays for.
	 */
	public Days( int newYear ) {
		_year = newYear;
		_holidayNames = createHolidayNames();
	}

	/**
	 * The year to calculate holidays for.
	 */
	public int year() {
		return _year;
	}

	/**
	 * @return A map containing all holidays and their names
	 */
	private Map<LocalDate, String> createHolidayNames() {
		final Map<LocalDate, String> m = new HashMap<>();

		m.put( newYearsDay(), "Nýársdagur" );
		m.put( maundyThursday(), "Skírdagur" );
		m.put( goodFriday(), "Föstudagurinn langi" );
		m.put( easterDay(), "Páskadagur" );
		m.put( easterMonday(), "Annar í páskum" );
		m.put( firstDayOfSummer(), "Sumardagurinn fyrsti" );
		m.put( firstOfMay(), "1. maí" );
		m.put( ascensionDay(), "Uppstigningardagur" );
		m.put( whiteMonday(), "Annar í hvítasunnu" );
		m.put( seventeenthOfJune(), "17. júní" );
		m.put( trademensDay(), "Frídagur verslunarmanna" );
		m.put( christmasEve(), "Aðfangadagur" );
		m.put( christmasDay(), "Jóladagur" );
		m.put( boxingDay(), "Annar í jólum" );
		m.put( newYearsEve(), "Gamlársdagur" );

		return m;
	}

	/**
	 * @return Name of the holiday falling on the given date if we have one, null if not a holiday
	 */
	public String holidayName( final LocalDate date ) {
		return _holidayNames.get( date );
	}

	/**
	 * All Icelandic holiday dates, partial and complete. Note that some of these holidays do not last the whole day. To get the days that last a whole day use fullHoldays. To get the days that last part of the day use partialHolidays.
	 *
	 * @return All Icelandic holidays
	 */
	public List<LocalDate> allHolidays() {
		List<LocalDate> a = new ArrayList<>();

		a.add( newYearsDay() );
		a.add( maundyThursday() );
		a.add( goodFriday() );
		a.add( easterDay() );
		a.add( easterMonday() );
		a.add( firstDayOfSummer() );
		a.add( firstOfMay() );
		a.add( ascensionDay() );
		a.add( whiteMonday() );
		a.add( seventeenthOfJune() );
		a.add( trademensDay() );
		a.add( christmasEve() );
		a.add( christmasDay() );
		a.add( boxingDay() );
		a.add( newYearsEve() );

		return a;
	}

	/**
	 * @return Icelandic full day holidays
	 */
	public List<LocalDate> fullHolidays() {
		List<LocalDate> a = new ArrayList<>();

		a.add( newYearsDay() );
		a.add( maundyThursday() );
		a.add( goodFriday() );
		a.add( easterDay() );
		a.add( easterMonday() );
		a.add( firstDayOfSummer() );
		a.add( firstOfMay() );
		a.add( ascensionDay() );
		a.add( whiteMonday() );
		a.add( seventeenthOfJune() );
		a.add( trademensDay() );
		a.add( christmasDay() );
		a.add( boxingDay() );

		return a;
	}

	/**
	 * @return Partial Icelandic holidays. (holiday after noon).
	 */
	public List<LocalDate> partialHolidays() {
		List<LocalDate> a = new ArrayList<>();

		a.add( christmasEve() );
		a.add( newYearsEve() );

		return a;
	}

	/**
	 * @param date The date to check
	 * @return true if the date is a holiday
	 */
	public static boolean isHoliday( LocalDate date ) {
		return new Days( date.getYear() ).allHolidays().contains( date );
	}

	/**
	 * @param date The date to check
	 * @return true if the date is a full holiday
	 */
	public static boolean isFullHoliday( LocalDate date ) {
		return new Days( date.getYear() ).fullHolidays().contains( date );
	}

	/**
	 * @param date The date to check
	 * @return true if the date is a full holiday
	 */
	public static boolean isPartialHoliday( LocalDate date ) {
		return new Days( date.getYear() ).partialHolidays().contains( date );
	}

	/**
	 * @return New Year's Day (Icelandic: Nýársdagur)
	 */
	public LocalDate newYearsDay() {
		return LocalDate.of( year(), 1, 1 );
	}

	/**
	 * @return Maundy Thursday (Icelandic: Skírdagur). Thursday before Easter Sunday.
	 */
	public LocalDate maundyThursday() {
		return easterDay().minusDays( 3 );
	}

	/**
	 * @return Good Friday (Icelandic: Föstudagurinn langi). Friday before Easter Sunday.
	 */
	public LocalDate goodFriday() {
		return easterDay().minusDays( 2 );
	}

	/**
	 * @return Easter Day (Icelandic: Páskadagur)
	 *
	 *         Borrowed from: http://www.smart.net/~mmontes/nature1876.html
	 *
	 *         Holds for any year in the Gregorian Calendar, which (of course) means years including and after 1583.
	 */
	public LocalDate easterDay() {
		int a = year() % 19;
		int b = year() / 100;
		int c = year() % 100;
		int d = b / 4;
		int e = b % 4;
		int f = (b + 8) / 25;
		int g = (b - f + 1) / 3;
		int h = (19 * a + b - d - g + 15) % 30;
		int i = c / 4;
		int k = c % 4;
		int l = (32 + 2 * e + 2 * i - h - k) % 7;
		int m = (a + 11 * h + 22 * l) / 451;
		int month = ((h + l - 7 * m + 114) / 31);
		int p = (h + l - 7 * m + 114) % 31;
		int day = p + 1;

		return LocalDate.of( year(), month, day );
	}

	/**
	 * @return Easter Monday (Icelandic: Annar í páskum). Monday following Easter Sunday
	 */
	public LocalDate easterMonday() {
		return easterDay().plusDays( 1 );
	}

	/**
	 * @return First day of summer (Icelandic: Sumardagurinn fyrsti). Thursday during the period 19 to 25 April
	 */
	public LocalDate firstDayOfSummer() {
		return LocalDate.of( year(), Month.APRIL, 18 ).with( TemporalAdjusters.next( DayOfWeek.THURSDAY ) );
	}

	/**
	 * @return Labour day (Icelandic: Verkalýðdagurinn (1. maí))
	 */
	public LocalDate firstOfMay() {
		return LocalDate.of( year(), 5, 1 );
	}

	/**
	 * @return Ascension day (Icelandic: Uppstigningardagur). Holy Thursday, six weeks after Maundy Thursday.
	 *
	 *         FIXME: Use easterDay() instead?
	 */
	public LocalDate ascensionDay() {
		return maundyThursday().plusDays( 42 );
	}

	/**
	 * @return White Monday (Icelandic: Annar í hvítasunnu). Monday following White Sunday, seven weeks after Easter.
	 */
	public LocalDate whiteMonday() {
		return easterMonday().plusDays( 49 );
	}

	/**
	 * @return Seventeenth of June (Icelandic: Þjóðhátíðardagurinn( 17. júní)). Icelandic national holiday.
	 */
	public LocalDate seventeenthOfJune() {
		return LocalDate.of( year(), 6, 17 );
	}

	/**
	 * @return Trademen's Day (Icelandic: Frídagur verslunarmanna). First Monday in August.
	 */
	public LocalDate trademensDay() {
		return LocalDate.of( year(), Month.AUGUST, 1 ).with( TemporalAdjusters.nextOrSame( DayOfWeek.MONDAY ) );
	}

	/**
	 * @return Christmas Eve (Icelandic: Aðfangadagur). Afternoon only.
	 */
	public LocalDate christmasEve() {
		return LocalDate.of( year(), 12, 24 );
	}

	/**
	 * @return Christmas Day (Icelandic: Jóladagur).
	 */
	public LocalDate christmasDay() {
		return LocalDate.of( year(), 12, 25 );
	}

	/**
	 * @return Boxing Day (Icelandic: Annar í jólum).
	 */
	public LocalDate boxingDay() {
		return LocalDate.of( year(), 12, 26 );
	}

	/**
	 * @return New Year's Eve (Icelandic: Gamlársdagur). Afternoon only.
	 */
	public LocalDate newYearsEve() {
		return LocalDate.of( year(), 12, 31 );
	}
}
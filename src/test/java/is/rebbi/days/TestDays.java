package is.rebbi.days;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import is.rebbi.days.Days;

/**
 * Tests for Holidays
 *
 * FIXME: Add more tests, from lists of known dates. Preferable for the entire 20th century and then some.
 */

public class TestDays {

	@Test
	public void isHoliday() {
		assertTrue( Days.isHoliday( LocalDate.of( 2000, 1, 1 ) ) );
		assertFalse( Days.isHoliday( LocalDate.of( 2000, 1, 2 ) ) );
		assertTrue( Days.isHoliday( LocalDate.of( 2014, 4, 20 ) ) );
		assertTrue( Days.isHoliday( LocalDate.of( 2010, 12, 24 ) ) );
	}

	@Test
	public void newYearsDay() {
		assertEquals( LocalDate.of( 2000, 1, 1 ), new Days( 2000 ).newYearsDay() );
	}

	@Test
	public void easterDay() {
		assertEquals( LocalDate.of( 2009, 4, 12 ), new Days( 2009 ).easterDay() );
		assertEquals( LocalDate.of( 2013, 3, 31 ), new Days( 2013 ).easterDay() );
	}

	@Test
	public void easterMonday() {
		assertEquals( LocalDate.of( 2009, 4, 13 ), new Days( 2009 ).easterMonday() );
		assertEquals( LocalDate.of( 2013, 4, 1 ), new Days( 2013 ).easterMonday() );
	}

	@Test
	public void maundyThursday() {
		assertEquals( LocalDate.of( 2009, 4, 9 ), new Days( 2009 ).maundyThursday() );
		assertEquals( LocalDate.of( 2013, 3, 28 ), new Days( 2013 ).maundyThursday() );
	}

	@Test
	public void goodFriday() {
		assertEquals( LocalDate.of( 2009, 4, 10 ), new Days( 2009 ).goodFriday() );
		assertEquals( LocalDate.of( 2013, 3, 29 ), new Days( 2013 ).goodFriday() );
	}

	@Test
	public void firstDayOfSummer() {
		assertEquals( LocalDate.of( 2009, 4, 23 ), new Days( 2009 ).firstDayOfSummer() );
		assertEquals( LocalDate.of( 2012, 4, 19 ), new Days( 2012 ).firstDayOfSummer() );
	}

	@Test
	public void firstOfMay() {
		assertEquals( LocalDate.of( 1900, 5, 1 ), new Days( 1900 ).firstOfMay() );
		assertEquals( LocalDate.of( 2009, 5, 1 ), new Days( 2009 ).firstOfMay() );
		assertEquals( LocalDate.of( 2012,  5,  1 ), new Days( 2012 ).firstOfMay() );
	}

	@Test
	public void ascensionDay() {
		assertEquals( LocalDate.of( 2009, 5, 21 ), new Days( 2009 ).ascensionDay() );
		assertEquals( LocalDate.of( 2004, 5, 20 ), new Days( 2004 ).ascensionDay() );
	}

	@Test
	public void whiteMonday() {
		assertEquals( LocalDate.of( 2009, 6, 1 ), new Days( 2009 ).whiteMonday() );
		assertEquals( LocalDate.of( 2012, 5, 28 ), new Days( 2012 ).whiteMonday() );
	}

	@Test
	public void seventeenthOfJune() {
		assertEquals( LocalDate.of( 2009, 6, 17 ), new Days( 2009 ).seventeenthOfJune() );
		assertEquals( LocalDate.of( 2012, 6, 17 ), new Days( 2012 ).seventeenthOfJune() );
	}

	@Test
	public void trademensDay() {
		assertEquals( LocalDate.of( 2009, 8, 3 ), new Days( 2009 ).trademensDay() );
		assertEquals( LocalDate.of( 2013, 8, 5 ), new Days( 2013 ).trademensDay() );
	}

	@Test
	public void christmasEve() {
		assertEquals( LocalDate.of( 2009, 12, 24 ), new Days( 2009 ).christmasEve() );
		assertEquals( LocalDate.of( 2013, 12, 24 ), new Days( 2013 ).christmasEve() );
	}

	@Test
	public void christmasDay() {
		assertEquals( LocalDate.of( 2009, 12, 25 ), new Days( 2009 ).christmasDay() );
		assertEquals( LocalDate.of( 2013, 12, 25 ), new Days( 2013 ).christmasDay() );
	}

	@Test
	public void boxingDay() {
		assertEquals( LocalDate.of( 2009, 12, 26 ), new Days( 2009 ).boxingDay() );
		assertEquals( LocalDate.of( 2013, 12, 26 ), new Days( 2013 ).boxingDay() );
	}

	@Test
	public void newYearsEve() {
		assertEquals( LocalDate.of( 2009, 12, 31 ), new Days( 2009 ).newYearsEve() );
		assertEquals( LocalDate.of( 2013, 12, 31 ), new Days( 2013 ).newYearsEve() );
	}
}